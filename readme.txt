Start Client:
    gradlew client:bootRun

Start Server:
    gradlew server:bootRun

Start Parallel:
    gradlew bootRun -parallel


