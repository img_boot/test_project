package test.project

class BootStrap {

    RecordService recordService

    def init = { servletContext ->
        recordService.initRecords()
    }

    def destroy = {
    }
}
