package test.project

import grails.rest.Resource

@Resource(uri='/records', readOnly = true)
class Record {
    String name
    String date
    BigDecimal xchangeRate


    static constraints = {
        name blank: false, nullable: false
        date blank: false, nullable: false
        xchangeRate blank: false, nullable: false
    }
}
