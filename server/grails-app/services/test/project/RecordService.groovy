package test.project

import grails.gorm.transactions.Transactional

import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Transactional
class RecordService {

    def dictionary = [
            R01235: 'USD',
            R01239: 'EUR'
    ]


    def initRecords() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern('dd/MM/yyyy')
        def date = LocalDate.now().minusMonths(1)
        def dateForm = date.withDayOfMonth(1).format(formatter)
        def dateTo = date.withDayOfMonth(date.lengthOfMonth()).format(formatter)

        dictionary.keySet().collect {
            'http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=' + dateForm + '&date_req2=' + dateTo + '&VAL_NM_RQ=' + it
        }.each {
            storeRecords(it)
        }
    }


    def storeRecords(String uri) {

        new XmlSlurper().parse(uri).childNodes().collect {
            def record = new Record()
            record.name = dictionary.get(it.attributes.get('Id'))
            record.date = it.attributes.get('Date')
            record.xchangeRate = new BigDecimal(it.childNodes().find { it.name == 'Value' }.children.get(0).replace(',', '.'))
            record.save flush: true
        }
    }

}