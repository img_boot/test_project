package test.project

class UrlMappings {

    static mappings = {
        "/"(view: 'application/index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
