export interface IRecord {
    id: string;
    name: string;
    date: string;
    xchangeRate: string;
}