import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {IRecord} from './record'
import 'rxjs/add/operator/map'

@Injectable()
export class RecordService {
    constructor(private _http: Http) {
    }

    getRecords(): Observable<IRecord[]> {
        return this._http
            .get('http://localhost:8080/records?max=-1')
            .map((response: Response) => <IRecord[]> response.json());
    }
}
