import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {RecordService} from './app.service'
import {IRecord} from "./record";
import {DomSanitizer} from '@angular/platform-browser';
import {RowClassArgs} from '@progress/kendo-angular-grid';
import {utf8Encode} from "@angular/compiler/src/util";

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

    constructor(private _recordService: RecordService, private sanitizer: DomSanitizer) {
    }

    period: Array<String>;
    values: Array<any> = [];
    records: IRecord[];
    errorMessage: string;

    ngOnInit() {
        this.getRecords();
    }

    getRecords() {
        this._recordService.getRecords()
            .subscribe(
                records => {
                    this.records = records;
                    this.period = records.map(rec => rec.date).filter((value, index, self) => self.indexOf(value) === index);

                    records.map(rec => rec.name)
                        .filter((value, index, self) => self.indexOf(value) === index)
                        .map(recName =>
                            this.values.push(
                                {
                                    name: recName,
                                    xchangeRate: records.filter(item => item.name === recName).map(item => item.xchangeRate)
                                }
                            )
                        );

                },
                error => this.errorMessage = <any>error,
            );
    }

    public colorCode(code: number, recName: string) {
        let result;
        switch (code) {
            case this.calcMin(recName):
                result = "#FFBA80";
                break;
            case this.calcMax(recName):
                result = "#B2F699";
                break;
            default:
                result = "transparent";
                break;
        }

        return this.sanitizer.bypassSecurityTrustStyle(result);
    }


    public  calcMin(recName) {
        return Math.min.apply(null, this.records.filter(rec => rec.name === recName).map(rec => rec.xchangeRate))
    }

    public  calcMax(recName) {
        return Math.max.apply(null, this.records.filter(rec => rec.name === recName).map(rec => rec.xchangeRate))
    }
}
